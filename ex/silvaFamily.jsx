import React from 'react'
import Member from './member'

export default () => (

	<div>
		<Member name='João' lastName='Silva'/>
		<Member name='Fabiana' lastName='Silva'/>
		<Member name='Antônio' lastName='Silva'/>
		<Member name='Pedro' lastName='Silva'/>
	</div>

)